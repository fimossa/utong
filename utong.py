import requests
from bs4 import BeautifulSoup
import pandas as pd 

def utong2(code):
    code = str(code)
    if len(code) != 6:
        tmp = ''
        for k in range(6-len(code)):
            tmp = '0' + tmp 
        code = tmp + code
    return code

def utong(code):
    code = str(code)
    if len(code) != 6:
        tmp = ''
        for k in range(6-len(code)):
            tmp = '0' + tmp 
        code = tmp + code 

    html = 'https://navercomp.wisereport.co.kr/v2/company/c1070001.aspx?cmp_cd=' + code + '&cn='
    req = requests.get(html)
    html = req.text
    soup = BeautifulSoup(html, 'html.parser')
    #
    comany_name = soup.select('#pArea > div.wrapper-table > div > table > tr > td > dl > dt > span')   
    #catogory = soup.select('#pArea > div.wrapper-table > div > table > tr > td > dl  [class]')
    catogory = soup.select('#pArea > div.wrapper-table > div > table > tr > td > dl > dt:nth-child(3)')
    #pArea > div.wrapper-table > div > table > tbody > tr:nth-child(1) > td > dl > dt:nth-child(2)
    utong_data = soup.select('#cTB711 > tbody > tr > td.center.noline-right')
#    print(my_titles)
#    my_titles2 = my_titles.select('td')[9]

    #print(catogory)
          
          
          
    t_list = str(utong_data).split('\t')
    t_result = ''
    if '%' in t_list[len(t_list)-1]:
        t_result = t_list[len(t_list)-1].split('%')[0]
    return [code,str(comany_name).split('>')[1].split('<')[0],str(catogory).split('>')[1].split('<')[0],t_result]
    
code_df = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13', header=0)[0]
result2 = []
for i in range(len(code_df)):
    t_code = utong2(code_df['종목코드'][i])
    result2.append(utong(t_code))
    print(t_code)
    
import csv
path = 'D://David//Documents//01_project//25_주식파이썬//'
file_name = 'utong.csv'
t_num = 1
with open(path + file_name, 'w', newline='') as csvfile:
    fieldnames = ['count','code','name','catagory','utong-mulyang']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for obj in result2:
        writer.writerow({'count' : t_num, 'code' : obj[0],'name' : obj[1], 'catagory' : obj[2], 'utong-mulyang' : obj[3]})
        t_num = t_num + 1